
import checkCars from '../../hooks/check-cars';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkCars()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
