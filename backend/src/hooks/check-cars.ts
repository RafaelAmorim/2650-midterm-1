// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { data } = context;
    
    if(!data.make){
      throw new Error('A make must be informed');
    }else if(!data.model){
      throw new Error('A model must be informed');
    }
    
    if(data.mileage <= 0){
      throw new Error('Mileage should be greater than 0');
    }
    
    if(data.year < 1885 || data.year > 2020){
      throw new Error('Only years from 1885 and 2020 are accepted');
    }
    
    return context;
  };
}
